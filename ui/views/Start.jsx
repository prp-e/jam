import React, {useState, useMemo} from 'react';
import slugify from 'slugify';

import {navigate} from '../lib/use-location';
import Container from './Container';
import {useJam} from '../jam-core-react';

export default function Start({newRoom = {}, urlRoomId, roomFromURIError}) {
  const [, {enterRoom, setProps, createRoom}] = useJam();

  // note: setters are currently unused because form is hidden
  let [name, setName] = useState(newRoom.name ?? '');
  let [description, setDescription] = useState(newRoom.description ?? '');
  let [color, setColor] = useState(newRoom.color ?? '#4B5563');
  let [logoURI, setLogoURI] = useState(newRoom.logoURI ?? '');
  let [buttonText, setButtonText] = useState(newRoom.buttonText ?? '');
  let [buttonURI, setButtonURI] = useState(newRoom.buttonURI ?? '');
  let {stageOnly = false} = newRoom;

  let [showAdvanced, setShowAdvanced] = useState(false);

  let submit = e => {
    e.preventDefault();
    setProps('userInteracted', true);
    let roomId;
    if (name) {
      let slug = slugify(name, {lower: true, strict: true});
      roomId = slug + '-' + Math.random().toString(36).substr(2, 4);
    } else {
      roomId = Math.random().toString(36).substr(2, 6);
    }

    (async () => {
      let roomPosted = {name, description, logoURI, color, stageOnly};
      let ok = await createRoom(roomId, roomPosted);
      if (ok) {
        if (urlRoomId !== roomId) navigate('/' + roomId);
        enterRoom(roomId);
      }
    })();
  };

  let humins = useMemo(() => {
    let humins = ['DoubleMalt', 'mitschabaude', '__tosh'];
    return humins.sort(() => Math.random() - 0.5);
  }, []);

  return (
    <Container style={{height: 'initial', minHeight: '100%'}}>
        <h1>Don't have permission</h1>

        <p className="text-gray-600">
          Making a room isn't possible through this frontend service. 
        </p>

        <hr className="mt-14 mb-14" />
    </Container>
  );
}
